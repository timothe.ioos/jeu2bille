package fr.timioos.jeudebillegyro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Boule boule;
    private Sensor capteur;
    private double gx;
    private double gz;
    private final Handler handler = new Handler();
    private boolean isGameOver = false;

    private Cible cible;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        capteur = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        ConstraintLayout layout = findViewById(R.id.activity_main);


        cible = new Cible(getApplicationContext());
        layout.addView(cible);

        boule = new Boule(getApplicationContext());
        layout.addView(boule);




        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float screenWidth = displayMetrics.widthPixels;
        float screenHeight = displayMetrics.heightPixels;

        while (!(cible.x > 200 && cible.x < screenWidth - 200 && cible.y >  200 && cible.y < screenHeight - 200)){
            cible.x = (float) (Math.random()*screenWidth);
            cible.y = (float) (Math.random()*screenHeight);
        }


        boule.x = (int) (screenWidth / 2);
        boule.y = (int) (screenHeight / 2);

        handler.postDelayed(runnable, 100);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateBoulePosition();
            handler.postDelayed(this, 100);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, capteur, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(this, capteur);
        super.onPause();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float gx = 0.0f, gy = 0.0f, gz = 0.0f;

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            gz = sensorEvent.values[0];
            gy = sensorEvent.values[1];
            gx = sensorEvent.values[2];
        }
        this.gx = gx;
        this.gz = gz;
    }
    private void updateBoulePosition() {

        if (!isGameOver) {

            float vitesseX = (float) -(gz * 6.0f);
            float vitesseY = (float) -(gx * 6.0f);

            boule.x += vitesseX;
            boule.y += vitesseY;

            boule.postInvalidate();

            if(cible.detectWin(boule)){
                isGameOver = true;
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), EndActivity.class);
                intent.putExtra("state", getString(R.string.win));
                startActivity(intent);
                finish();
            }

            if (boule.sortieEcran()) {
                isGameOver = true;
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), EndActivity.class);
                intent.putExtra("state", getString(R.string.game_over));
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}