package fr.timioos.jeudebillegyro;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.View;

public class Boule extends View {
    Paint pinceau;
    public int x,y;


    public Boule(Context context) {
        super(context);
        pinceau = new Paint();
        pinceau.setColor(Color.BLUE);


    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int rayon = 50;
        canvas.drawCircle(x, y, rayon, pinceau);
    }


    /*
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        String[] nomCouleurs = {"vert", "bleu", "jaune"};
        int[] couleurs = {Color.GREEN, Color.BLUE, Color.YELLOW};
        int hasard = (int)(Math.random()*nomCouleurs.length);
        pinceau.setColor(couleurs[hasard]);
        x = (int)event.getX();
        y = (int)event.getY();
        invalidate();
        requestLayout();
        String position = "(" + x + ", " + y + ")";
        Toast.makeText(getContext(),
                "touché en " + position +"\n" +nomCouleurs[hasard],
                Toast.LENGTH_SHORT).show();
        return super.onTouchEvent(event);
    } */


    public boolean sortieEcran() {
        // Récupérer les dimensions de l'écran
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float screenWidth = displayMetrics.widthPixels;
        float screenHeight = displayMetrics.heightPixels;

        // Vérifier si la boule est sortie de l'écran
        if (x < 0 || x > screenWidth || y < 0 || y > screenHeight) {
            // Afficher un message dans la console
            return true;
        }
        return false;
    }

}
