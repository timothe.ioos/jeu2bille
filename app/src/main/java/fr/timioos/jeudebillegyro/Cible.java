package fr.timioos.jeudebillegyro;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class Cible extends View {
    Paint pinceau;
    public float x,y = 0;


    public Cible(Context context) {
        super(context);
        pinceau = new Paint();
        pinceau.setColor(Color.RED);

    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(x,y,x+200,y+200, pinceau);
    }

    public boolean detectWin(Boule boule){
        return boule.x > x && boule.x < x + 200 && boule.y > y && boule.y < y + 200;
    }

}


